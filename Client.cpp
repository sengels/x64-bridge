// client.cpp
#define _WIN32_DCOM
#include <iostream>
#include "Server.h"

using namespace std;

void main()
{
	CoInitializeEx(NULL, COINIT_MULTITHREADED);		
	IUnknown* pUnknown = 0;
	IDouble* pDouble = 0;	
	COSERVERINFO si; 
	char szHost[64];			// "\\\\localhost"
	WCHAR wcsHost[64];
	cout << "Type in the name of the machine hosting the server \n(e.g. \\\\localhost):" << endl;
	cin >> szHost;
	size_t t = mbstowcs( wcsHost, szHost, 64 );
	ZeroMemory(&si, sizeof(si));
	si.pwszName = wcsHost;		//L "\\\\Walter"; 
	MULTI_QI rgmqi[1];
	ZeroMemory(rgmqi, sizeof(rgmqi));
	rgmqi[0].pIID = &IID_IUnknown;
	HRESULT hr = CoCreateInstanceEx(
//		CLSID_MiniDcom, NULL, CLSCTX_REMOTE_SERVER, &si, 1, rgmqi);
		CLSID_MiniDcom, NULL, CLSCTX_LOCAL_SERVER, &si, 1, rgmqi);
	if (hr == S_OK)
		pUnknown  = (IUnknown*)rgmqi[0].pItf;
	else if(hr == REGDB_E_CLASSNOTREG)
	{
		cout << "CoCreateInstanceEx failed. 1" << endl;
		return;
	}
	else if(hr == CLASS_E_NOAGGREGATION)
	{
		cout << "CoCreateInstanceEx failed. 2" << endl;
		return;
	}
	else if(hr == CO_S_NOTALLINTERFACES)
	{
		cout << "CoCreateInstanceEx failed. 3" << endl;
		return;
	}
	else if(hr == E_NOINTERFACE)
	{
		cout << "CoCreateInstanceEx failed. 4" << endl;
		return;
	}
	hr = pUnknown->QueryInterface(IID_IDouble, (void**)&pDouble); 
	if(FAILED(hr))
	{
		cout << "QueryInterface FAILED" << endl;
		return;
	}
	cout << "VC DCOM Client started." << endl;
	hr = pUnknown->Release();
	int iInput, iDouble;
	cout << "type in a integer:" << endl;
	cin >> iInput;
	hr = pDouble->Double(iInput, &iDouble);
	if(SUCCEEDED(hr))
		cout << iInput <<" * 2 = " << iDouble << endl << "\"q\" to exit" << endl;
	cin >> iInput;
	hr = pDouble->Release();
	CoUninitialize();
	cout << "Client exited." << endl;
}