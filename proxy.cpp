// just so it links

#if 0
#define INC_OLE2

#include <windows.h>
#include <windowsx.h>
#include <shlobj.h>
#include <stdio.h>
#include <stdlib.h>
#if defined(_MSC_VER) || defined(__MINGW64__)
#include <ShellApi.h>
#endif

#define GUID_SIZE 128
//#define ResultFromShort(i) ResultFromScode(MAKE_SCODE(SEVERITY_SUCCESS, 0, (USHORT)(i)))

//#pragma data_seg(".text")
#define INITGUID
#include <initguid.h>
#include <shlguid.h>
#include "Server.h"
//#pragma data_seg()

typedef struct{
  HKEY  hRootKey;
  LPTSTR szSubKey;
  LPTSTR lpszValueName;
  LPTSTR szData;
} DOREGSTRUCT;

BOOL RegisterServer(CLSID, LPTSTR);
BOOL UnregisterServer(CLSID, LPTSTR);

HINSTANCE _hModule = NULL; // DLL Module.
TCHAR szShellExtensionTitle[] = TEXT("Kate");

BOOL RegisterServer(CLSID clsid, LPTSTR lpszTitle)
{
  int      i;
  HKEY     hKey;
  LRESULT  lResult;
  DWORD    dwDisp;
  TCHAR    szSubKey[MAX_PATH];
  TCHAR    szCLSID[MAX_PATH];
  TCHAR    szModule[MAX_PATH];
  LPOLESTR   pwsz;

  StringFromIID(clsid, &pwsz);
  if(pwsz) {
    lstrcpy(szCLSID, pwsz);
    //free the string
    LPMALLOC pMalloc;
    CoGetMalloc(1, &pMalloc);
    pMalloc->Free(pwsz);
    pMalloc->Release();
  }

  //get this app's path and file name
  GetModuleFileName(_hModule, szModule, MAX_PATH);

  DOREGSTRUCT ClsidEntries[] = {
    HKEY_CLASSES_ROOT,   TEXT("Wow6432Node\\Interface\\%s"),                          NULL,                   "IDouble",
    HKEY_CLASSES_ROOT,   TEXT("Wow6432Node\\CLSID\\%s"),                              NULL,                   lpszTitle,
//    HKEY_CLASSES_ROOT,   TEXT("Wow6432Node\\CLSID\\%s\\InprocServer32"),              NULL,                   szModule,
//    HKEY_CLASSES_ROOT,   TEXT("Wow6432Node\\CLSID\\%s\\InprocServer32"),              TEXT("ThreadingModel"), TEXT("Apartment"),
//    HKEY_CLASSES_ROOT,   TEXT("*\\shellex\\ContextMenuHandlers\\Kate"),  NULL,                   szCLSID,
    NULL,                NULL,                                           NULL,                   NULL
  };
  OutputDebugString(TEXT("test\n"));
  // Register the CLSID entries
  for(i = 0; ClsidEntries[i].hRootKey; i++) {
    // Create the sub key string - for this case, insert the file extension
    wsprintf(szSubKey, ClsidEntries[i].szSubKey, szCLSID);
    OutputDebugString(szSubKey);
    lResult = RegCreateKeyEx(ClsidEntries[i].hRootKey, szSubKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKey, &dwDisp);
    if(NOERROR == lResult) {
        TCHAR szData[MAX_PATH];
        // If necessary, create the value string
        wsprintf(szData, ClsidEntries[i].szData, szModule);
        lResult = RegSetValueEx(hKey, ClsidEntries[i].lpszValueName, 0, REG_SZ, (LPBYTE)szData, (lstrlen(szData) + 1) * sizeof(TCHAR));
        RegCloseKey(hKey);
    } else {
        OutputDebugString(TEXT("we couldn't write a registry key!\n"));
        return FALSE;
    }
  }
  return TRUE;
}

STDAPI DllRegisterServer(void)
{
    return (RegisterServer(CLSID_MiniDcom, szShellExtensionTitle) ? S_OK : E_FAIL);
}

STDAPI DllUnregisterServer(void)
{
    return 0;
}
#endif